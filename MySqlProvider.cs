using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeliverySignOff.Models;
using MySql.Data.MySqlClient; 

public class MySqlProvider : IProvider
{
    public IList<Item> Items {get;}
    public List<Person> AllEmployees { get; private set; }
    private Random random = new Random();

    private const string mySqlHost = "db";
    private const string mySqlUsername = "root";
    private readonly string mySqlPwd = Environment.GetEnvironmentVariable("DBPASS");

    public OrderContext CreateOrder(Guid id, IList<Item> items)
    {
        var chef = this.AllEmployees[random.Next(AllEmployees.Count-1)];
            chef.Role = new Role { Description = "Preparer"};
            var deliverer = this.AllEmployees[random.Next(AllEmployees.Count-1)];
            deliverer.Role = new Role { Description = "deliverer"};

        return new OrderContext(){
            Id = id,
            Items = items,
            Handlers = new  List<Person>{ chef, deliverer }
        };
    }

    public async Task AddEmployeeDataAsync(Person employee)
    {
        string cs = $"server={mySqlHost};userid=root;password={mySqlPwd};database=delivery_sign_off";
        using (var conn = new MySqlConnection(cs))
        {
            await conn.OpenAsync();
            using(var cmd = conn.CreateCommand())
            {
                cmd.CommandText = @"insert into persons(id,roleid,body_temperature,firstname,lastname)
                values (?id, ?roleid, ?bodyTemperature, ?firstname, ?lastname)";
                cmd.Parameters.AddRange(new[]{
                    new  MySqlParameter("id", Guid.NewGuid()),
                    new MySqlParameter("roleid", employee.Role.RoleId),
                    new MySqlParameter("bodyTemperature", employee.BodyTemperature),
                    new MySqlParameter("firstname", employee.FirstName),
                    new MySqlParameter("lastname",employee.LastName)
                });

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }

    public async Task<IList<Role>> GetAllRolesAsync()
    {
        string cs = $"server={mySqlHost};userid=root;password={mySqlPwd};database=delivery_sign_off";
        using (var conn = new MySqlConnection(cs))
        {
            await conn.OpenAsync();
            var list = new List<Role>();
            using(var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "select * from roles_master";
                using(var reader = await cmd.ExecuteReaderAsync()){
                    while(await reader.ReadAsync()){
                        list.Add(new Role{ 
                            RoleId = (int) reader["id"],
                            Description = reader["description"].ToString()
                         });

                    }
                    return list;
                }
            }
        }
    }

    public MySqlProvider() : base(){

        Items = new List<Item>();

        string cs = $"server={mySqlHost};userid=root;password={mySqlPwd};database=delivery_sign_off";
        using (var conn = new MySqlConnection(cs))
        {
            conn.Open();
            using(var cmd = new MySqlCommand("select * from items", conn))
            using(var rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    Items.Add(new Item{
                        Id = Guid.Parse(rdr["id"].ToString()),
                        Description = rdr["description"].ToString(),
                        Price = (Decimal)rdr["price"]
                    });
                }
            }
        }

        AllEmployees = new List<Person>();

        for (int i = 0; i< 100; i++)
        {
            AllEmployees.Add(
                new Person{
                    PersonId = Guid.NewGuid(),
                    FirstName = Faker.Name.First(),
                    LastName = Faker.Name.Last(),
                    BodyTemperature = 97.4M,
                    TraveledAbroadInLast14Days = false
                }
            );
        }
    }
}