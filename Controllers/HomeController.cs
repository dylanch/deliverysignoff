﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DeliverySignOff.Models;

namespace DeliverySignOff.Controllers
{
    public class HomeController : Controller
    {
        private IProvider provider;// = new Provider();
        private Random random = new Random();

        public HomeController(IProvider provider){
            this.provider = provider;
        }

        public IActionResult Index()
        {
            return View(provider.CreateOrder(Guid.NewGuid(), provider.Items ));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllRolesAsync()
        {
            return Json(await provider.GetAllRolesAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployeeAsync([FromBody] Person employee )
        {
            await provider.AddEmployeeDataAsync(employee);
            return Accepted();
        }
        public IActionResult POSClient()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
