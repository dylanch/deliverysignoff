using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeliverySignOff.Models;

/// replace this class with a class that does actual database transactions
// and then in Startup.cs => services.AddTransient<IProvider,{new type name}>()
public class Provider : IProvider
{
    public IList<Item> Items { get;}
    public IList<Person> AllEmployees {get;}

    private Random random = new Random();

    public OrderContext CreateOrder(Guid id, IList<Item> items)
    {
        var chef = this.AllEmployees[random.Next(AllEmployees.Count-1)];
            chef.Role = new Role { Description = "Preparer"};
            var deliverer = this.AllEmployees[random.Next(AllEmployees.Count-1)];
            deliverer.Role = new Role { Description = "deliverer"};

        return new OrderContext(){
            Id = id,
            Items = items,
            Handlers = new  List<Person>{ chef, deliverer }
        };
    }

    public Task AddEmployeeDataAsync(Person employee)
    {
        throw new NotImplementedException();
    }

    public Task<IList<Role>> GetAllRolesAsync()
    {
        throw new NotImplementedException();
    }

    public Provider()
    {
        Items = new List<Item>();

        Items.Add( new Item {
            Description = "Cheeseburger",
            Price = 4.99M,
            Id = Guid.Parse("381231f7-93cb-44f4-88d9-168a8528ea92")
        });

        Items.Add( new Item {
            Description = "Chicken sandwich",
            Price = 4.99M,
            Id = Guid.Parse("9014e476-7229-47a7-85c9-580a2b702b14")
        });

        Items.Add( new Item{
            Description = "French Fries",
            Price = 2.99M,
            Id = Guid.Parse("9e8ab8ae-95e8-4d7c-a4df-c6afa361531d")
        });

        AllEmployees = new List<Person>();

        for (int i = 0; i< 100; i++)
        {
            AllEmployees.Add(
                new Person{
                    PersonId = Guid.NewGuid(),
                    FirstName = Faker.Name.First(),
                    LastName = Faker.Name.Last(),
                    BodyTemperature = 97.4M,
                    TraveledAbroadInLast14Days = false
                }
            );
        }
    }
}