using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeliverySignOff.Models;

public interface IProvider
{
    IList<Item> Items{get;}
    OrderContext CreateOrder(Guid id, IList<Item> items);
    Task AddEmployeeDataAsync(Person employee);

    Task<IList<Role>> GetAllRolesAsync();
}