'use strict';
class HandlerList extends React.Component {

    constructor(props){
        super(props);
        this.props = props;
        this.state = {
            handlers : props.handlers
        }
    }

    render() {
        let handlers = this.state.handlers;
        
        let listItems = handlers.map( x => 
            React.createElement('li', { className: 'list-group-item', key : x.PersonId},  [
                    React.createElement('span',{style:{fontWeight:'bold'}},`${x.FirstName} ${x.LastName} (${x.Role})`),
                    React.createElement('table',{ className : 'table'},
                    React.createElement('tbody', null, [
                        React.createElement('tr',{},[
                            React.createElement('td',null,'Body temperature'),
                            React.createElement('td',null, 
                                React.createElement('span',{className: x.BodyTemperature< 98.4 ? 'text-success' : 'text-danger'},x.BodyTemperature)
                            )
                        ]),
                        React.createElement('tr',{},[
                            React.createElement('td',null,'Travled abroad in the last 14 days'),
                            React.createElement('td',null, React.createElement('span',{className: x['TraveledAbroadInLast14Days'] ? 'badge badge-danger' : 'badge badge-success'}, x['TraveledAbroadInLast14Days']? 'Yes' : 'No'))
                        ])
                    ]))
                ]
            )  
        );
        return React.createElement('ul', { className: 'list-group' }, listItems)
    }
} 