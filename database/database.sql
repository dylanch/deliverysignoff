CREATE DATABASE delivery_sign_off;
use delivery_sign_off;

CREATE table items(id varchar(40) not null, 
    description varchar(200) unique, 
    price decimal(15,2), 
    constraint pk_items primary key (id));

CREATE table roles_master(
    id integer not null,
    description varchar(200),
    constraint pk_roles primary key (id)
);

CREATE table persons(
    id varchar(40) not null, 
    roleid integer REFERENCES roles_master(roleid), 
    body_temperature decimal(4,1),
    firstname varchar(20),
    lastname varchar(30),
    constraint pk_persons primary key (id));

insert into items(id, description, price) values (UUID(), 'Cheeseburger', 4.99);
insert into items(id, description, price) values (UUID(), 'Beef burrito', 4.99);
insert into items(id, description, price) values (UUID(), 'Spaghetti meatballs', 4.99);

insert into roles_master(id, description) values(1, 'Delivery');
insert into roles_master(id, description) values(2, 'Chef/Preparer');
insert into roles_master(id, description) values(3, 'Other');