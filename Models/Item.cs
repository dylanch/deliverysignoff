using System;

namespace DeliverySignOff.Models{
    public class Item
    {
        public Guid Id { get; set; }
        public string Description {get; set;}
        public decimal Price {get; set;}
    }
}