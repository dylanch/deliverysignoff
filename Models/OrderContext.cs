using System;
using System.Collections.Generic;

namespace DeliverySignOff.Models{
    public class OrderContext
    {
        public Guid Id { get;set; }
        public IList<Item> Items {get; set;} 
        public IList<Person> Handlers {get; set;}
        public string Customer {get; set; }
    }
}