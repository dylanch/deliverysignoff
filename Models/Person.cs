using System;

namespace DeliverySignOff.Models{
    public class Person
    {
        public Guid PersonId {get; set;}
        public Role Role {get; set;}
        public decimal BodyTemperature {get; set;}
        public bool TraveledAbroadInLast14Days {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
    }

    public class Role{
        public int RoleId {get;set;}
        public string Description{get;set;}
    }
}